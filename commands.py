from drawings import Line, Circle

class BaseCommand:
    SAVE_TO_HISTORY = True
    WORK_WITH_SELECTION = False

    def execute(self):
        pass

    def undo(self):
        pass
    
class CreateLineCommand(BaseCommand):
    INPUT_POINT_COUNT = 2
    
    def __init__(self, window, points, options=None):
        self.window = window
        self.points = points
        self.options = options

    def execute(self):
        new_drawing = Line(self.points, self.options)
        self.window.drawings.append(new_drawing)

    def undo(self):
        self.window.drawings.pop()

class CreateCircleCommand(BaseCommand):
    INPUT_POINT_COUNT = 2
    
    def __init__(self, window, points, options=None):
        self.window = window
        self.points = points
        self.options = options

    def execute(self):
        new_drawing = Circle(self.points, self.options)
        self.window.drawings.append(new_drawing)

    def undo(self):
        self.window.drawings.pop()

class UndoCommand(BaseCommand):
    INPUT_POINT_COUNT = 0
    SAVE_TO_HISTORY = False
    
    def __init__(self, window, points, options=None):
        self.window = window
        self.points = points
        self.options = options

    def execute(self):
        if len(self.window.command_history) > 0:
            last_command = self.window.command_history.pop()
            last_command.undo()


class SelectCommand(BaseCommand):
    INPUT_POINT_COUNT = 1
    SAVE_TO_HISTORY = False
    
    def __init__(self, window, points, options=None):
        self.window = window
        self.points = points
        self.options = options

    def execute(self):
        n = len(self.window.drawings)

        for i in range(n):
            j = n - 1 - i
            if self.window.drawings[j].is_close(self.points[0][0],
                                                self.points[0][1]):
                self.window.drawings[j].is_selected = True
                break


class DeleteSelectedCommand(BaseCommand):
    INPUT_POINT_COUNT = 0
    SAVE_TO_HISTORY = True
    WORK_WITH_SELECTION = True

    
    def __init__(self, window, points, options=None):
        self.window = window
        self.points = points
        self.options = options

    def execute(self):
        new_drawings = []
        for d in self.window.drawings:
            if not d.is_selected:
                new_drawings.append(d)

        self.old_drawings = self.window.drawings
        self.window.drawings = new_drawings

    def undo(self):
        self.window.drawings = self.old_drawings
