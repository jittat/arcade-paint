import arcade
import math

CLOSE_THRESHOLD = 5

class Line:
    def __init__(self, points, options=None):
        self.x1 = points[0][0]
        self.y1 = points[0][1]
        self.x2 = points[1][0]
        self.y2 = points[1][1]

        self.is_selected = False

        if options and 'color' in options:
            self.color = options['color']
        else:
            self.color = arcade.color.BLACK

    def draw(self):
        if self.is_selected:
            arcade.draw_line(self.x1, self.y1,
                             self.x2, self.y2,
                             arcade.color.GRAY,
                             line_width=5)
            
        arcade.draw_line(self.x1, self.y1,
                         self.x2, self.y2,
                         self.color)
            
        
    def is_close(self, x, y):
        ux = self.x2 - self.x1
        uy = self.y2 - self.y1
        vx = x - self.x1
        vy = y - self.y1

        ulen = math.sqrt(ux*ux + uy*uy)
        uxu = ux / ulen
        uyu = uy / ulen

        wlen = uxu * vx + uyu * vy
        if wlen < -CLOSE_THRESHOLD or wlen > ulen + CLOSE_THRESHOLD:
            return False

        wx = uxu * wlen
        wy = uyu * wlen
        return math.sqrt((vx - wx)**2 + (vy - wy)**2) <= CLOSE_THRESHOLD

    
class Circle:
    def __init__(self, points, options=None):
        self.x = points[0][0]
        self.y = points[0][1]

        self.rx = points[1][0]
        self.ry = points[1][1]

        self.is_selected = False

        self.radius = math.sqrt((self.x - self.rx) ** 2
                                + (self.y - self.ry) ** 2)
        
    def draw(self):
        if self.is_selected:
            arcade.draw_circle_outline(self.x, self.y,
                                       self.radius,
                                       arcade.color.GRAY,
                                       border_width=5)
            
        arcade.draw_circle_outline(self.x, self.y,
                                   self.radius,
                                   arcade.color.BLACK)

        
    def is_close(self, x, y):
        r = math.sqrt((self.x - x)**2 +
                      (self.y - y)**2)

        return (self.radius - CLOSE_THRESHOLD <= r <= self.radius + CLOSE_THRESHOLD)
            

